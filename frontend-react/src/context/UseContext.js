import { createContext, useState } from "react";

const Context = createContext({})

export function UseContext({ children }) {
    const [user, setUser] = useState(() => {
        const user = window.sessionStorage.getItem('user')
        return JSON.parse(user)
    });
    const [favourites, setFavourites] = useState(() => {
        const favourites = window.sessionStorage.getItem('favourites')
        return JSON.parse(favourites)
    });

    return (
        <Context.Provider value={{ user, setUser, favourites, setFavourites }}>
            { children }
        </Context.Provider>
    )
}

export default Context