import { useContext } from "react";
import Context from "../context/UseContext";


const useUser = () => {
    const { user, setUser } = useContext(Context)
    
    const login = ({ email, password }) => {
        window.sessionStorage.setItem('user', JSON.stringify(email))
        setUser(email)
    }

    const logout = () => {
        window.sessionStorage.removeItem('user')
        window.sessionStorage.removeItem('favourites')
        setUser(null)
    }

    return {
        login,
        logout,

        user,
        isLogged: Boolean(user)
    }
}
 
export default useUser;