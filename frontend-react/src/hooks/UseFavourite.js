import { useContext } from "react";
import Context from "../context/UseContext";


const useFavourite = () => {
    const { favourites, setFavourites } = useContext(Context)

    const addToFavourite = ({ newFavName }) => {
        const favouiteList = favourites != null ? favourites : [];
        favouiteList.push(newFavName)
        setFavourites(favouiteList)

        window.sessionStorage.setItem('favourites', JSON.stringify(favouiteList))
    }

    const removeToFavourite = ({ favName }) => {
        const newFavList = favourites.filter(favourites => favourites != favName)
        window.sessionStorage.setItem('favourites', JSON.stringify(newFavList))
        setFavourites(newFavList)
    }

    const isInFavourites = ({ name }) => {
        const fav = JSON.parse(window.sessionStorage.getItem('favourites'))
        return Boolean(fav && fav.find(favName => favName === name))
    }

    return {
        favourites,

        addToFavourite,
        removeToFavourite,
        isInFavourites
    }
}
 
export default useFavourite;