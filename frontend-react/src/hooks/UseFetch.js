import { useEffect, useState } from "react"

export const UseFetch = (url) => {
    const [res, setRes] = useState({ isLoading: false, data: null })

    useEffect(() => {
        getData(url)
    }, [url])

    function getData(url) {
        setRes({ isLoading:true, data: null})

        fetch(url)
        .then(res => res.json())
        .then(res => setRes({ isLoading:false, data: res}))
    }

    return res
}