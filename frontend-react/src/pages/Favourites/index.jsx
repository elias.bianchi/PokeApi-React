import { useEffect, useState } from "react";
import CardList from "../../components/CardPokemon/CardList";
import useFavourite from "../../hooks/UseFavourite";
import { UseFetch } from "../../hooks/UseFetch";

const Favourites = () => {
    const res = UseFetch("https://pokeapi.co/api/v2/pokemon?limit=50")
    const {isLoading, data} = res;
    const { favourites } = useFavourite()
    const [pokemonList, setPokemonList] = useState([]); 

    useEffect(() => {
        const favList = data?.results.filter(pokemon => favourites?.includes(pokemon.name))
        setPokemonList(favList)
    }, [data])

    const doNothing = () => {}

    return (
        <div className='pokemonList'>
            {
                isLoading 
                ? <p>cargando</p> 
                : (
                    pokemonList?.length ? (
                        <CardList
                            list={pokemonList}
                            setShowList={doNothing}
                            setPokemonUrl={doNothing}
                        />
                    ) : (
                        <h2>No hay favoritos</h2>
                    )
                )
            }
        </div>
    );
}
 
export default Favourites;