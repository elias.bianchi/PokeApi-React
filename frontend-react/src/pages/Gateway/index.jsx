import { useState } from "react";
import Login from "./Login";
import Register from "./Register";


const Gateway = () => {
    const [login, setLogin] = useState(true);

    const toggleLogin = () => {
         setLogin(!login)
    };

    return ( 
        <div className="gateway">
            {
                login ? (
                    <div className="login">
                        <Login
                            toggleLogin={toggleLogin}
                        />
                    </div>
                ) : (
                    <div className="register">
                        <Register
                            toggleLogin={toggleLogin}
                        />
                    </div>
                )
            }
            
        </div>
    );
}
 
export default Gateway;