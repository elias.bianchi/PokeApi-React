import { useEffect, useRef } from "react";
import LoginForm from "./LoginForm";
import useLogin from "../../../hooks/UseUser";
import { useNavigate } from "react-router-dom";
import './Login.scss'

const Login = (props) => {
	const navigate = useNavigate()
    const { login, isLogged } = useLogin()
	const isMountedRef = useRef(null)

    useEffect(() => {
        isMountedRef.current = true
		if(isMountedRef.current && isLogged){
			navigate('/home', { replace: true })
		}

		return function cleanUp () {
			isMountedRef.current = false
		}
    }, [isLogged])

    const handleLogin = ({ email, password }) => {
        login({ email })
    }

    return (
        <div className="login">
            <div className="container">
                <h1>Login</h1>
                <LoginForm login={handleLogin}/>
                <button onClick={() => props.toggleLogin()}>Check In</button>
            </div>
        </div>
    );
}
 
export default Login;