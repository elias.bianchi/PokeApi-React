import { useState } from "react";


const LoginForm = (props) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const login = (e) => {
        e.preventDefault()

        props.login({ email: email.trim(), password })
    }

    return (
        <form>
            <input type="text" name="email"
                placeholder="Email"
                value={ email }
                onChange={e => setEmail(e.target.value)}
            />
            <input type="password" name="password" 
                placeholder="Password"
                value={ password }
                onChange={e => setPassword(e.target.value)}
            />
            <button onClick={login}>Login</button>
        </form>
    );
}
 
export default LoginForm;