import React, { useEffect, useState } from 'react'
import './Home.scss'
import { UseFetch } from '../../hooks/UseFetch'
import { CardPokemon } from '../../components/CardPokemon/CardPokemon';
import Searcher from '../../components/Searcher/Searcher';
import CardList from '../../components/CardPokemon/CardList';

function Home() {
    const res = UseFetch("https://pokeapi.co/api/v2/pokemon?limit=50")
    const {isLoading, data} = res;
    const [ShowList, setShowList] = useState(true)
    const [pokemonUrl, setPokemonUrl] = useState("")
    const [pokemonList, setPokemonList] = useState([]); 

    useEffect(() => {
        setPokemonList(data?.results)
    }, [data])

    const back = () => {
        setPokemonUrl("")
        setShowList(true)
    }

    const doNothing = () => {}

    return (
        <div className='Home'>
            {
                !ShowList ? (   
                    <div className='pokemonInfo'>
                    {pokemonUrl.length > 0 && (
                        <div>
                            <CardPokemon
                                url={pokemonUrl}
                                setShowList={doNothing}
                                setPokemonUrl={doNothing}
                            />
                            <button onClick={back}>Regresar a la lista</button>
                        </div>
                    )}
                    </div>
                ) : (
                    <>
                        <Searcher 
                            allPokemons={data?.results}
                            setPokemonList={setPokemonList}
                        />

                        <div className='pokemonList'>
                            {
                                isLoading 
                                ? <p>cargando</p> 
                                : (
                                    pokemonList && (
                                        <CardList
                                            list={pokemonList}
                                            setShowList={setShowList}
                                            setPokemonUrl={setPokemonUrl}
                                        />
                                    )
                                )
                            }
                        </div>
                    </>
                )
            }
        </div>
    )
}

export default Home