import { Routes, Route } from 'react-router-dom'
import './App.scss';
import NotFound from './components/NotFound';
import { UseContext as UseContextProvider } from './context/UseContext';
import Gateway from './pages/Gateway';
import Home from './pages/Home';
import Header from './components/Header';
import PrivateRoute from './components/PrivateRoute';
import Favourites from './pages/Favourites';

function App() {

  return (
    <UseContextProvider>
      <Header />
      <Routes>
        <Route exact path='/' element={ <Gateway /> } />
        <Route exact path='/home' element={ <PrivateRoute><Home /></PrivateRoute> } />
        <Route exact path='/favourites' element={ <PrivateRoute><Favourites /></PrivateRoute> } />
        <Route exact path='*' element={ <NotFound /> } />
      </Routes>
    </UseContextProvider>
  );
}

export default App;
