import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import useFavourite from "../../hooks/UseFavourite";
import { UseFetch } from "../../hooks/UseFetch";
import './CardPokemon.scss'

export const CardPokemon = ({ url, setShowList, setPokemonUrl }) => {
    const res = UseFetch(url)
    const { addToFavourite, removeToFavourite, isInFavourites } = useFavourite()
    const { isLoading, data } = res;
    const [isFavourite, setIsFavourite] = useState(false)

    useEffect(() => {
        data && setIsFavourite(isInFavourites({ name: data.name }))
    }, [data])

    const view = (e) => {
        setPokemonUrl(url)
        setShowList(false)
    }

    const toggleFavourtie = () => {
        
        isInFavourites({ name: data.name }) 
            ? removeToFavourite({ favName: data.name}) 
            : addToFavourite({ newFavName: data.name })

        setIsFavourite(!isFavourite)
    }

    return (
        <>
            {isLoading ? <p>Cargando</p> : data && (
                <div className="card">
                    <div className="favouriteSwitch">
                        <input
                            type="checkbox"
                            id={`switch${data.id}`}
                            checked={isFavourite}
                            onChange={toggleFavourtie}
                        />
                        <label htmlFor={`switch${data.id}`}>★</label>
                    </div>

                    <div className="cardContent" onClick={() => view()}>
                        <img src={data.sprites.front_default} alt="" />
                        <h3>{data.name.charAt(0).toUpperCase() + data. name.slice(1)}</h3>
                        <div className="abilities">
                            {data.abilities.map((ability, key) => (
                                <div key={key}>
                                    {ability.ability.name}
                                </div>
                            ))}
                        </div>
                    </div> 
                </div>
            )}
        </>
    )
}