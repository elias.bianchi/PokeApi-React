import { CardPokemon } from "./CardPokemon";

const CardList = ({ list, setShowList, setPokemonUrl }) => {
    return ( 
        <>
            {
                list?.map((pokemon, key) => (
                    <CardPokemon 
                        key={key}
                        url={pokemon.url}
                        setShowList={setShowList}
                        setPokemonUrl={setPokemonUrl}
                    />
                ))
            }
        </>
     );
}
 
export default CardList;