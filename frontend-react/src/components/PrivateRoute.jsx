import useLogin from "../hooks/UseUser";
import Login from "../pages/Gateway/Login";

const PrivateRoute = ({ children }) => {
    const { isLogged } = useLogin()

    return isLogged 
        ? (
            <div className='base'>
                { children }
            </div>
        )
        : <Login />
}
 
export default PrivateRoute;