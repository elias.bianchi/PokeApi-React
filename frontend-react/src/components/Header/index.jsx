import { useNavigate } from 'react-router-dom';
import useLogin from '../../hooks/UseUser';
import logo from '../../Pokebola.png'
import './Header.scss'

const Header = () => {
	const navigate = useNavigate()
	const { user, isLogged, logout } = useLogin()

  const handleLogout = () => {
    logout()
    navigate('/', { replace: true })
  }

  return (
      <div className="navBar">
        <div className='inputs'>
          <img src={logo} alt="PokemonLogo" />
          <button onClick={() => navigate('/home', { replace: true })}>Pokemon List</button>
          {
            isLogged && <button onClick={() => navigate('/favourites', { replace: true })}>Favourites</button>
          }
        </div>

        <label>Por: Elias Bianchi</label>
        {
          isLogged && (
            <>
              <label>{ user }</label>
              <button onClick={handleLogout}>Salir</button>
            </>
          )
        }
    </div>
  );
}
 
export default Header;