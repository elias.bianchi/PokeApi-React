import { useState } from 'react';
import './Searcher.scss'

const Searcher = ({ allPokemons, setPokemonList }) => {
    const [filterWorld, setFilterWorld] = useState("");

    const filter = (e) => {
        e.preventDefault()
        
        if (filterWorld.length > 2 ){
            let filteredList = allPokemons.filter(pokemon => pokemon.name.toLowerCase().includes(filterWorld.toLowerCase()))
            setPokemonList(filteredList)
        } else {
            setPokemonList(allPokemons)
        }
    }

    return ( 
        <div className="searcher">
            <form action="">
                <input 
                    type="text" 
                    name="" 
                    id="" 
                    value={filterWorld}
                    onChange={(e) => {
                        setFilterWorld(e.target.value)
                        filter(e)
                    }}
                />
                <button onClick={(e) => filter(e)}>Buscar</button>
            </form>
        </div>
    );
}
 
export default Searcher;